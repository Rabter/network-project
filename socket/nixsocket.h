#ifndef NIXSOCKET_H
#define NIXSOCKET_H

#include "socket.h"
#include <sys/socket.h>
#include <stdlib.h>
#include <unistd.h>


class NixSocket: public Socket
{
public:
    NixSocket(sa_family_t address_family, int type, int protocol);

    int fd() override;

    int bind(unsigned short port) override;
    int listen(int backlog) override;
    int select(int n, void *readfds, void *writefds, void *exceptfds, void *timeout) override;
    int connect(const std::string &ip, unsigned short port) override;
    Socket* accept(std::string &ip, unsigned short &port) override;

    int send(const std::string &msg, int flags = 0) override;
    int sendto(const std::string &ip, unsigned short port, const std::string &msg, int flags = 0) override;
    int recv(char *msg, int len, int flags) override;
    int recvfrom(char *msg, int len, int flags, std::string &ip, unsigned short &port) override;

    int setsockopt(int level, int optname, const void *optval, int optlen) override;

    void FdZero(void *fds) override;
    void FdSet(void *fds) override;
    int FdIsSet(void *fds) override;

    ~NixSocket() override;

private:
    int sock;
    sa_family_t af;
    NixSocket(int fd, int adress_family);
};

#endif // NIXSOCKET_H
