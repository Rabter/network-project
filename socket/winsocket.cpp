#ifdef _WIN32

#include "winsocket.h"

WinSocket::WinSocket(int address_family, int socket_type, int protocol): Socket()
{
    WSAStartup(MAKEWORD(2, 2), &ws);
    sock = socket(address_family, socket_type, protocol);
    af = address_family;
}

int WinSocket::fd()
{
    return sock;
}

int WinSocket::bind(unsigned short port)
{
    sockaddr_in saddr;
    ZeroMemory(&saddr, sizeof(saddr));
    saddr.sin_family = af;
    saddr.sin_addr.s_addr = INADDR_ANY;
    saddr.sin_port = htons(port);
    return ::bind(sock, (sockaddr*)&saddr, sizeof(saddr));
}

int WinSocket::listen(int backlog)
{
    return ::listen(sock, backlog);
}

int WinSocket::select(int n, void *readfds, void *writefds, void *exceptfds, void *timeout)
{
    return ::select(n, (fd_set*)readfds, (fd_set*)writefds, (fd_set*)exceptfds, (timeval*)timeout);
}

int WinSocket::connect(const std::string &ip, unsigned short port)
{
    sockaddr_in saddr;
    ZeroMemory(&saddr, sizeof(saddr));
    saddr.sin_family = af;
    saddr.sin_addr.s_addr = inet_addr(ip.c_str());
    saddr.sin_port = htons(port);
    return ::connect(sock, (sockaddr *)&saddr, sizeof (saddr));
}

Socket* WinSocket::accept(std::string &ip, unsigned short &port)
{
    sockaddr_in client;
    int client_size = sizeof(client);
    WinSocket *accepted = new WinSocket(::accept(sock, (sockaddr*)&client, &client_size), af);
    ip = inet_ntoa(client.sin_addr);
    port = client.sin_port;
    return accepted;
}

int WinSocket::send(const std::string &msg, int flags)
{
    return ::send(sock, msg.c_str(), msg.length() + 1, flags);
}

int WinSocket::sendto(const std::string &ip, unsigned short port, const std::string &msg, int flags)
{
    sockaddr_in saddr;
    ZeroMemory(&saddr, sizeof(saddr));
    saddr.sin_family = af;
    saddr.sin_addr.s_addr = inet_addr(ip.c_str());
    saddr.sin_port = htons(port);
    return ::sendto(sock, msg.c_str(), msg.length() + 1, flags, (sockaddr*)&saddr, sizeof(saddr));
}

int WinSocket::recv(char *msg, int len, int flags)
{
    return ::recv(sock, msg, len, flags);
}

int WinSocket::recvfrom(char *msg, int len, int flags, std::string &ip, unsigned short &port)
{
    sockaddr_in sender_addr;
    int addrlen = sizeof(sender_addr);
    int rc = ::recvfrom(sock, msg, len, flags, (sockaddr*)&sender_addr, &addrlen);
    ip = inet_ntoa(sender_addr.sin_addr);
    port = sender_addr.sin_port;
    return rc;
}

int WinSocket::setsockopt(int level, int optname, const void *optval, int optlen)
{
    return ::setsockopt(sock, level, optname, (char*)optval, optlen);
}

void WinSocket::FdZero(void *fds)
{
    FD_ZERO((fd_set*)fds);
}

void WinSocket::FdSet(void *fds)
{
    FD_SET(sock, (fd_set*)fds);
}

int WinSocket::FdIsSet(void *fds)
{
    return FD_ISSET(sock, (fd_set*)fds);
}

WinSocket::~WinSocket()
{
    closesocket(sock);
}

WinSocket::WinSocket(int fd, int address_family)
{
    WSAStartup(MAKEWORD(2, 2), &ws);
    sock = fd;
    af = address_family;
}

#endif
