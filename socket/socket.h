#ifndef SOCKET_H
#define SOCKET_H


#include <string>

class Socket
{
public:

    Socket() = default;

    virtual int fd() = 0;

    virtual int bind(unsigned short port) = 0;
    virtual int listen(int backlog) = 0;
    virtual int select(int n, void *readfds, void *writefds, void *exceptfds, void *timeout) = 0;
    virtual int connect(const std::string &ip, unsigned short port) = 0;
    virtual Socket* accept(std::string &ip, unsigned short &port) = 0;

    virtual int send(const std::string &msg, int flags = 0) = 0;
    virtual int sendto(const std::string &ip, unsigned short port, const std::string &msg, int flags = 0) = 0;
    virtual int recv(char *msg, int len, int flags) = 0;
    virtual int recvfrom(char *msg, int len, int flags, std::string &ip, unsigned short &port) = 0;

    virtual int setsockopt(int level, int optname, const void *optval, int optlen) = 0;

    virtual void FdZero(void *fds) = 0;
    virtual void FdSet(void *fds) = 0;
    virtual int FdIsSet(void *fds) = 0;

    virtual ~Socket() = default;
};

#endif // SOCKET_H
