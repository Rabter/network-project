#ifndef _WIN32

#include "nixsocket.h"
#include <sys/types.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include <string.h>


NixSocket::NixSocket(sa_family_t address_family, int type, int protocol): Socket()
{
    af = address_family;
    sock = socket(address_family, type, protocol);
}

int NixSocket::fd()
{
    return sock;
}

int NixSocket::bind(unsigned short port)
{
    sockaddr_in saddr;
    memset(&saddr, 0, sizeof(saddr));
    saddr.sin_family = af;
    saddr.sin_addr.s_addr = INADDR_ANY;
    saddr.sin_port = htons(port);
    return ::bind(sock, (sockaddr*)&saddr, sizeof(saddr));
}

int NixSocket::listen(int backlog)
{
    return ::listen(sock, backlog);
}

int NixSocket::select(int n, void *readfds, void *writefds, void *exceptfds, void *timeout)
{
    return ::select(n, (fd_set*)readfds, (fd_set*)writefds, (fd_set*)exceptfds, (timeval*)timeout);
}

int NixSocket::connect(const std::string &ip, unsigned short port)
{
    sockaddr_in saddr;
    memset(&saddr, 0, sizeof(saddr));
    saddr.sin_family = af;
    saddr.sin_addr.s_addr = inet_addr(ip.c_str());
    saddr.sin_port = htons(port);
    return ::connect(sock, (sockaddr *)&saddr, sizeof (saddr));
}

Socket* NixSocket::accept(std::string &ip, unsigned short &port)
{
    sockaddr_in client;
    socklen_t client_size = sizeof(client);
    NixSocket* accepted = new NixSocket(::accept(sock, (sockaddr*)&client, &client_size), af);
    ip = inet_ntoa(client.sin_addr);
    port = client.sin_port;
    return accepted;
}

int NixSocket::send(const std::string &msg, int flags)
{
    return ::send(sock, msg.c_str(), msg.length() + 1, flags);
}

int NixSocket::sendto(const std::string &ip, unsigned short port, const std::string &msg, int flags)
{
    sockaddr_in saddr;
    memset(&saddr, 0, sizeof(saddr));
    saddr.sin_family = af;
    saddr.sin_addr.s_addr = inet_addr(ip.c_str()); //inet_aton(SRV_IP, &server_addr.sin_addr)
    saddr.sin_port = htons(port);
    return ::sendto(sock, msg.c_str(), msg.length() + 1, flags, (sockaddr*)&saddr, sizeof(saddr));
}

int NixSocket::recv(char *msg, int len, int flags)
{
    return ::recv(sock, msg, len, flags);
}

int NixSocket::recvfrom(char *msg, int len, int flags, std::string &ip, unsigned short &port)
{
    sockaddr_in sender_addr;
    socklen_t addrlen = sizeof(sender_addr);
    int rc = ::recvfrom(sock, msg, len, flags, (sockaddr*)&sender_addr, &addrlen);
    ip = inet_ntoa(sender_addr.sin_addr);
    port = sender_addr.sin_port;
    return rc;
}

int NixSocket::setsockopt(int level, int optname, const void *optval, int optlen)
{
    return ::setsockopt(sock, level, optname, optval, optlen);
}

void NixSocket::FdZero(void *fds)
{
    FD_ZERO((fd_set*)fds);
}

void NixSocket::FdSet(void *fds)
{
    FD_SET(sock, (fd_set*)fds);
}

int NixSocket::FdIsSet(void *fds)
{
    return FD_ISSET(sock, (fd_set*)fds);
}

NixSocket::~NixSocket()
{
    close(sock);
}

NixSocket::NixSocket(int fd, int address_family)
{
    sock = fd;
    af = address_family;
}

#endif
