#ifndef CROSSPLATFORM_SOCKET_H
#define CROSSPLATFORM_SOCKET_H

#include "socket.h"

#ifdef _WIN32
#include "winsocket.h"
inline Socket *create_socket(int af, int type, int protocol = 0) { return new WinSocket(af, type, protocol); }
#else
#include "nixsocket.h"
inline Socket *create_socket(int af, int type, int protocol = 0) { return new NixSocket(af, type, protocol); }
#endif

#endif // CROSSPLATFORM_SOCKET_H
