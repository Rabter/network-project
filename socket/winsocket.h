#ifndef WINSOCKET_H
#define WINSOCKET_H

#include "socket.h"
#include "winsock2.h"

class WinSocket: public Socket
{
public:
    WinSocket(int address_family, int socket_type, int protocol);

    int fd() override;

    int bind(unsigned short port) override;
    int listen(int backlog) override;
    int select(int n, void *readfds, void *writefds, void *exceptfds, void *timeout) override;
    int connect(const std::string &ip, unsigned short port) override;
    Socket* accept(std::string &ip, unsigned short &port) override;

    int send(const std::string &msg, int flags = 0) override;
    int sendto(const std::string &ip, unsigned short port, const std::string &msg, int flags = 0) override;
    int recv(char *msg, int len, int flags) override;
    int recvfrom(char *msg, int len, int flags, std::string &ip, unsigned short &port) override;

    int setsockopt(int level, int optname, const void *optval, int optlen) override;

    void FdZero(void *fds) override;
    void FdSet(void *fds) override;
    int FdIsSet(void *fds) override;

    ~WinSocket();

private:
    WSADATA ws;
    SOCKET sock;
    int af;

    WinSocket(int fd, int address_family);
};

#endif // WINSOCKET_H
