#include "net.h"

std::string broadcast_ip(const std::string &ip, const std::string &mask)
{
    std::string broadcast;
    int ipnewp, masknewp, ipoldp = 0, maskoldp = 0;
    for (int i = 0; i < 4; ++i)
    {
        ipnewp = ip.find('.', ipoldp);
        masknewp = mask.find('.', maskoldp);
        unsigned char ip_octet = std::stoi(ip.substr(ipoldp, ipnewp - ipoldp));
        unsigned char mask_octet = std::stoi(mask.substr(maskoldp, masknewp - maskoldp));
        broadcast += std::to_string(int(static_cast<unsigned char>(ip_octet | ~mask_octet))) + ".";
        ipoldp = ipnewp + 1;
        maskoldp = masknewp + 1;
    }
    broadcast.pop_back();
    return broadcast;
}
