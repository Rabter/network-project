#ifdef _WIN32

#include <iphlpapi.h>
#include "net.h"


PIP_ADAPTER_INFO get_info()
{
    PIP_ADAPTER_INFO info;
    DWORD rc = NO_ERROR;
    ULONG ulOutBufLen = sizeof (IP_ADAPTER_INFO);
    info = new IP_ADAPTER_INFO;
    if (!info)
        rc = ERROR_OUTOFMEMORY;


    // Make an initial call to GetAdaptersInfo to get
    // the necessary size into the ulOutBufLen variable
    if (!rc && (rc = GetAdaptersInfo(info, &ulOutBufLen)) == ERROR_BUFFER_OVERFLOW)
    {
        delete info;
        info = new IP_ADAPTER_INFO[ulOutBufLen / sizeof(IP_ADAPTER_INFO)];
        if (!info)
            rc = ERROR_OUTOFMEMORY;
        rc = GetAdaptersInfo(info, &ulOutBufLen);
    }

    if (rc == NO_ERROR)
    {
        bool go = true;
        std::string high_addr;
        while (info && go)
        {
            high_addr = std::string(info->IpAddressList.IpAddress.String);
            high_addr.resize(7);
            if (high_addr == "192.168")
                go = false;
            else
                info = info->Next;
        }
        return info;
    }
    return NULL;
}

std::string local_ip()
{
    PIP_ADAPTER_INFO info = get_info();
    if (info)
        return std::string(info->IpAddressList.IpAddress.String);
    return "";
}

std::string subnet_mask()
{
    PIP_ADAPTER_INFO info = get_info();
    if (info)
        return std::string(info->IpAddressList.IpMask.String);
    return "";
}

#endif
