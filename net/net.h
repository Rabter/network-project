#ifndef NET_H
#define NET_H

#include <string>

std::string local_ip();
std::string subnet_mask();
std::string broadcast_ip(const std::string &ip, const std::string &mask);

#endif // NET_H
