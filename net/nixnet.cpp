#ifndef _WIN32

#include <string>
#include <ifaddrs.h>
#include <arpa/inet.h>
#include "net.h"

ifaddrs *get_info(ifaddrs *begin)
{
    bool go = true;
    std::string high_addr;
    while (begin && go)
    {
        sockaddr_in *saddr = (sockaddr_in*)begin->ifa_addr;
        high_addr = std::string(inet_ntoa(saddr->sin_addr));
        high_addr.resize(7);
        if (high_addr == "192.168")
            go = false;
        else
            begin = begin->ifa_next;
    }
    return begin;
}

std::string local_ip()
{
    ifaddrs *ifap, *local;
    getifaddrs(&ifap);
    local = get_info(ifap);
    if (!local)
        return "";
    sockaddr_in *info = (sockaddr_in*)local->ifa_addr;
    std::string ip = inet_ntoa(info->sin_addr);
    freeifaddrs(ifap);
    return ip;
}

std::string subnet_mask()
{
    ifaddrs *ifap, *local;
    getifaddrs(&ifap);
    local = get_info(ifap);
    if (!local)
        return "";
    sockaddr_in *info = (sockaddr_in*)local->ifa_netmask;
    std::string mask = inet_ntoa(info->sin_addr);
    freeifaddrs(ifap);
    return mask;
}

#endif
