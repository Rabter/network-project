#include <iostream>
#include <signal.h>
#include "host_connector/hostconnector.h"
#include "controller/controller.h"
#define COMMAND_MAXLEN 20

static bool go = true;
static HostConnector connector;

void sigint_handler(int sig)
{
    if(sig == SIGINT)
    {
        go = false;
        connector.disconnect();
        std::cout << "\nDisconnecting host and shutting down...\n";
        exit(0);
    }
}

int main()
{
    static Controller controller(&connector);
    char command[COMMAND_MAXLEN];
    int recieved;
    signal(SIGINT, sigint_handler);
    do
    {
        connector.wait_for_host();
        do
        {
            recieved = connector.recieve(command, COMMAND_MAXLEN);
            if (recieved)
                controller.handle_command(command);
            else
                std::cout << "Host has disconnected\n";
        } while (recieved);
        connector.disconnect();
    } while (true);
}
