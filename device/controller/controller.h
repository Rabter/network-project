#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <string>
#include "../protocol.h"
#include "../host_connector/hostconnector.h"
#define BUFFER_SIZE 100
#define SERVOS_AMOUNT 6

class Controller
{
public:
    Controller(HostConnector *connector);
    void handle_command(const std::string &command);
    void setup();

    ~Controller();

private:
    HostConnector *connector;

    char original_dir[BUFFER_SIZE];
    char script_path[BUFFER_SIZE];

    int servos[SERVOS_AMOUNT];
    int script_pid;
    int outpipe;
};

#endif // CONTROLLER_H
