#include <iostream>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include "controller.h"
#include "../../protocol.h"
#define CONTROL_SCRIPT_DIR "../arm_scripts"
#define CONTROL_SCRIPT_NAME "/control.py"
#define OUTPUT_PIPE "commandPipe"


Controller::Controller(HostConnector *connector): connector(connector)
{
    getcwd(original_dir, BUFFER_SIZE);
    chdir(CONTROL_SCRIPT_DIR);
    getcwd(script_path, BUFFER_SIZE);
    chdir(original_dir);
    strcpy(script_path + strlen(script_path), CONTROL_SCRIPT_NAME);
    if (!(script_pid = fork()))
        if(execlp("python3", "python3", script_path, NULL))
            perror("Exec failed: ");
    if (mkfifo(OUTPUT_PIPE, 0777))
        perror("Mkfifo failed:");
    if ((outpipe = open(OUTPUT_PIPE, O_WRONLY)) <= 0)
        perror("Open failed:");
    setup();
}

void Controller::handle_command(const std::string &command)
{
    std::cout << "Host has sent a command: " << command << '\n';
    if (command.find(GET) == 0)
    {
        std::size_t index = command.find(" ");
        std::string servo_number = command.substr(index);
        std::string response(GET_RESPONSE " " + std::to_string(servos[std::stoi(servo_number)]));
        std::cout << "Sending get response: " << response << '\n';
        if (connector->send(response) < 1)
            perror("Send failed");
    }
    else if (command.find(SET) == 0)
    {
        std::size_t space1 = command.find(" ") + 1;
        std::size_t space2 = command.find(" ", space1);
        std::string servo_number = command.substr(space1, space2 - space1);
        std::string angle = command.substr(space2 + 1);
        std::cout << "setting "<< servo_number << " to " << angle << "\n";
        std::string msg = "set " + servo_number + " " + angle;
        if (write(outpipe, msg.c_str(), msg.length() + 1) <= 0)
            perror("Write set failed:");
        servos[std::stoi(servo_number)] = std::stoi(angle);
    }
}

void Controller::setup()
{
    std::cout << "Setting up the device\n";
    std::string command = "setup";
    if (write(outpipe, command.c_str(), command.length() + 1) <= 0)
        perror("Write setup failed:");
    std::cout << "Setting up servos\n";
    for (int i = 0; i < SERVOS_AMOUNT; ++i)
    {
        command = "set " + std::to_string(i) + " 0";
        if (write(outpipe, command.c_str(), command.length() + 1) <= 0)
            perror("Write set failed:");
        servos[i] = 0;
    }
    std::cout << "The device is set up\n";
}

Controller::~Controller()
{
    std::cout << "Destroying constructor\n";
    char command[] = "exit";
    if (write(outpipe, command, sizeof(command)) <= 0)
        perror("Write exit failed:");
    close(outpipe);
    waitpid(script_pid, nullptr, 0);
}
