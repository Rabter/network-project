#ifndef HOSTCONNECTOR_H
#define HOSTCONNECTOR_H

#include "../socket/crossplatform_socket.h"

class HostConnector
{
public:
    HostConnector();
    void wait_for_host();

    void disconnect();

    int recieve(char *msg, int len, int flags = 0);
    int send(const std::string &msg, int flags = 0);

    ~HostConnector();

private:
    Socket *sock;
};

#endif // HOSTCONNECTOR_H
