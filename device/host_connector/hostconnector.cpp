#include <iostream>
#include "hostconnector.h"
#include "../protocol.h"
#include "../net/net.h"
#define DEVICE_NAME "Armsy robot"


HostConnector::HostConnector()
{
    sock = nullptr;
}

void HostConnector::wait_for_host()
{
    if (sock)
        return;

    Socket *muster_responser = create_socket(AF_INET, SOCK_DGRAM);
    Socket *listener = create_socket(AF_INET, SOCK_STREAM);

    int flag = 1;
    listener->setsockopt(SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag));

    if (muster_responser->bind(DEVICE_UDP_PORT))
    {
        perror("Failed to bind UDP socket.\n");
        exit(1);
    }

    if (listener->bind(DEVICE_TCP_PORT))
    {
        perror("Failed to bind TCP socket.\n");
        exit(1);
    }

    if (listener->listen(1))
    {
        perror("Error in listen():");
        exit(1);
    }

    std::cout << "Sockets are bound.\nTCP socket is listening on " << local_ip() << ':' << DEVICE_TCP_PORT << '\n';

    fd_set readfds;
    int max_fd = (muster_responser->fd() > listener->fd()? muster_responser->fd() : listener->fd()) + 1;

    do
    {
        muster_responser->FdZero(&readfds);
        muster_responser->FdSet(&readfds);
        listener->FdSet(&readfds);
        std::cout << "Calling select()...\n";
        int num = muster_responser->select(max_fd + 1, &readfds, nullptr, nullptr, nullptr);
        std::cout << "Select has returned " << num << " fds\n";
        if (num < 1)
        {
            perror("Select has returned a non-positive number\n");
            exit(1);
        }

        constexpr int size = sizeof(MUSTER_MSG) + 1;
        char msg[size];
        std::string master_ip = "";
        unsigned short master_port = 0;
        if (muster_responser->FdIsSet(&readfds))
        {
            muster_responser->recvfrom(msg, size, 0, master_ip, master_port);
            std::cout << "Recieved a message:\n" << msg << "\n";
            std::cout << "From: " << master_ip << ":" << master_port << "\n";
            if (std::string(msg) == MUSTER_MSG)
            {
                std::string response(MUSTER_RESPONSE);
                response += ' ';
                response += std::to_string(static_cast<int>(MECHANIZED_ARM));
                response += " " DEVICE_NAME;
                if (muster_responser->sendto(master_ip, MASTER_UDP_PORT, response))
                    std::cout << "Sent response: " << response << "\nTo: " << master_ip << ':' << MASTER_UDP_PORT << '\n';
            }
        }
    } while(!listener->FdIsSet(&readfds));

    std::string ip;
    unsigned short port;

    sock = listener->accept(ip, port);

    std::cout << "Accepted connection from " << ip << ':' << port << '\n';

    delete listener;
    delete muster_responser;
}

void HostConnector::disconnect()
{
    if (sock)
        delete sock;
    sock = nullptr;
}

int HostConnector::recieve(char *msg, int len, int flags)
{
    if (!sock)
        return  0;
    return sock->recv(msg, len, flags);
}

int HostConnector::send(const std::string &msg, int flags)
{
    if (!sock)
        return -1;
    return sock->send(msg, flags);
}

HostConnector::~HostConnector()
{
    disconnect();
}
