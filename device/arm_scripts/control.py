import os
import sys
import time
from adafruit_servokit import ServoKit

kit = ServoKit(channels=8)


def servo_get(num):
    angle = kit.servo[num].angle
    if num == 3:  # servo #3 is reversed on robo arm
        angle = 180 - angle
    return angle


def servo(num, angle):
    if num == 3:  # servo #3 is reversed on robo arm
        angle = 180 - angle
    kit.servo[num].angle = angle


def check_all():
    for i in range(6):
        kit.servo[i].angle = 0
        time.sleep(2)
        kit.servo[i].angle = 180
        time.sleep(2)
        kit.servo[i].angle = 0
        time.sleep(2)


def setup():
    kit.servo[0].set_pulse_width_range(min_pulse=620, max_pulse=2750)
    kit.servo[1].set_pulse_width_range(min_pulse=540, max_pulse=2750)
    kit.servo[2].set_pulse_width_range(min_pulse=600, max_pulse=2750)
    kit.servo[3].set_pulse_width_range(min_pulse=450, max_pulse=2650)
    kit.servo[4].set_pulse_width_range(min_pulse=600, max_pulse=2450)
    kit.servo[5].set_pulse_width_range(min_pulse=750, max_pulse=2750)


def proceed(command: str):
    args = command.split(' ')
    print("processing", command)
    if args[0] == "set":
        servo(int(args[1]), int(args[2]))
    elif args[0] == "get":
        return str(servo_get(int(args[1])))
    elif args[0] == "setup":
        setup()
    elif args[0] == "exit":
        sys.exit(0)
    else:
        print('Invalid command')
    return None


if __name__ == "__main__":
    input_pipe_name = "commandPipe"
    output_pipe_name = "resultPipe"
    if not os.path.exists(output_pipe_name):
        os.mkfifo(output_pipe_name)

    while not os.path.exists(input_pipe_name):
        pass    
    
    print("Opening pipe...")
    pipe = os.open(input_pipe_name, os.O_RDONLY)
    while True:
        try:
            print("Reading pipe...")
            msg = os.read(pipe, 1024)
            msg = msg.decode("UTF-8")
            print('Got: ' + msg)
            commands = msg.split('\x00')
            print(len(commands), 'commands')
            for command in commands:
                res = proceed(command)
                if res is not None:
                    pipe = os.open(output_pipe_name, os.O_WRONLY)
                    os.write(pipe, bytes(res, 'UTF-8'))
                    os.close(pipe)
            print('Finished commands')
        except KeyboardInterrupt:
            os.close(pipe)
            sys.exit(0)
