TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        ../socket/nixsocket.cpp \
        main.cpp \
    ../net/nixnet.cpp \
    ../net/net.cpp \
    host_connector/hostconnector.cpp \
    controller/controller.cpp

HEADERS += \
    ../protocol.h \
    ../socket/crossplatform_socket.h \
    ../socket/nixsocket.h \
    ../socket/socket.h \
    ../net/net.h \
    host_connector/hostconnector.h \
    controller/controller.h
