#include <chrono>
#include "devicecoordinator.h"
#include "../../net/net.h"
#include "../../protocol.h"
#define MAX_SELECT_TIME timeval{ 0, 200000 }

void DeviceCoordinator::scan_net(std::vector<Device> &devices, unsigned long usec_to_wait)
{
    Socket *sock_udp = create_socket(AF_INET, SOCK_DGRAM);

    devices.clear();

    int rc;
    rc = sock_udp->bind(MASTER_UDP_PORT);
    if (rc)
        perror("Error in bind(): ");

    int flag = 1;
    rc = sock_udp->setsockopt(SOL_SOCKET, SO_BROADCAST, &flag, sizeof(flag));
    if (rc)
        perror("Error in setsockopt(): ");

    sock_udp->sendto(broadcast_ip(local_ip(), subnet_mask()), DEVICE_UDP_PORT, MUSTER_MSG);

    char buf[MAX_DEV_MSG_LEN];

    timeval lim;
    lim.tv_sec = usec_to_wait / 1000000;
    lim.tv_usec = usec_to_wait % 1000000;

    unsigned long microseconds;

    std::string dev_ip, dev_name;
    unsigned short dev_port;
    std::chrono::time_point<std::chrono::system_clock> cur, begin = std::chrono::system_clock::now();
    int responded;
    fd_set readfds;
    do
    {
        sock_udp->FdZero(&readfds);
        sock_udp->FdSet(&readfds);
        if ((responded = sock_udp->select(sock_udp->fd() + 1, &readfds, NULL, NULL, &lim)))
        {
            rc = sock_udp->recvfrom(buf, sizeof(buf), 0, dev_ip, dev_port);
            if (rc == -1)
                perror("Error in recvfrom(): ");
            std::string response(buf);
            if (response.find(MUSTER_RESPONSE) == 0)
            {
                std::size_t space1 = response.find(' ');
                std::size_t space2 = response.find(' ', space1 + 1);
                DeviceType type = static_cast<DeviceType>(std::stoi(response.substr(space1 + 1, space2 - space1)));
                dev_name = response.substr(space2 + 1);
                devices.push_back(Device{ dev_ip, dev_name, type });
            }
        }
        cur = std::chrono::system_clock::now();
        microseconds = std::chrono::duration_cast<std::chrono::microseconds>(cur - begin).count();
    }while(microseconds < usec_to_wait || responded);

    delete sock_udp;
}

int DeviceCoordinator::connect(const std::string &ip)
{
    connected = true;
    sock = create_socket(AF_INET, SOCK_STREAM);
    return sock->connect(ip, DEVICE_TCP_PORT);
}

void DeviceCoordinator::disconnect()
{
    if (connected)
    {
        delete sock;
        sock = nullptr;
    }
    connected = false;
}

int DeviceCoordinator::get()
{
    int rc = -1;
    bool go = true;
    if (recieved_msgs.size())
    {
        for (std::size_t i = 0; go && i < recieved_msgs.size(); ++i)
        {
            if (recieved_msgs[i].find(GET_RESPONSE) == 0)
            {
                std::string msg = recieved_msgs[i];
                recieved_msgs.erase(recieved_msgs.begin() + i);
                rc = std::stoi(msg.substr(msg.find(' ') + 1));
                go = false;
            }
        }
    }
    if (go)
    {
        do {
            char buf[MAX_DEV_MSG_LEN];
            sock->recv(buf, MAX_DEV_MSG_LEN, 0);
            std::string msg = buf;
            if ((go = msg.find(GET_RESPONSE) != 0))
                recieved_msgs.push_back(msg); // Store a message for someone else
            else
                rc = std::stoi(msg.substr(msg.find(' ') + 1));
        } while (go);
    }
    return rc;
}

DeviceCoordinator::~DeviceCoordinator()
{
    disconnect();
}
