#ifndef SCAN_NET
#define SCAN_NET

#include <vector>
#include <string>
#include "device.h"
#include "../../socket/crossplatform_socket.h"

class DeviceCoordinator
{
public:
    inline DeviceCoordinator(): connected(false) {}

    void scan_net(std::vector<Device> &devices, unsigned long usec_to_wait = 2000000);

    int connect(const std::string &ip);
    inline bool is_connected() { return connected; }
    void disconnect();

    inline int send(std::string msg) { return sock->send(msg); }
    int get();

    ~DeviceCoordinator();

private:
    Socket *sock;
    bool connected;

    bool recieve;
    std::vector<std::string> recieved_msgs;
};

#endif // SCAN_NET
