#ifndef DEVICE_H
#define DEVICE_H

#include <string>
#include "../../protocol.h"

struct Device
{
    std::string ip, name;
    DeviceType type;
};

#endif // DEVICE_H
