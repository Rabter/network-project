#ifndef INTENTRY_H
#define INTENTRY_H

#include "validentry.h"

class IntEntry : public ValidEntry
{
public:
    IntEntry(QWidget *parent);
    bool get_int(int &num);
};

#endif // INTENTRY_H
