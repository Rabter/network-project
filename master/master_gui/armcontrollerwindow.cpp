#include <QKeyEvent>
#include "armcontrollerwindow.h"
#include "ui_armcontrollerwindow.h"
#include <QDebug>

ArmControllerWindow::ArmControllerWindow(QWidget *parent, DeviceCoordinator *coordinator) :
    QDialog(parent), ui(new Ui::ControllerWindow), coordinator(coordinator), controller(coordinator)
{
    ui->setupUi(this);
    enabled = false;
}

ArmControllerWindow::~ArmControllerWindow()
{
    delete ui;
}

void ArmControllerWindow::keyPressEvent(QKeyEvent *event)
{
    if (enabled || event->isAutoRepeat())
        event->ignore();
    else
    {
        qDebug() << "Key pressed\n";
        int key = event->key();
        bool go = true;
        for (int i = 0; i < 2 && go; ++i)
            for (int j = 0; j < 6 && go; ++j)
                if (servo_control[i][j] == key)
                {

                    if (i)
                        controller.decrease_move_factor(j);
                    else
                        controller.increase_move_factor(j);
                    go = false;
                }
    }
}

void ArmControllerWindow::keyReleaseEvent(QKeyEvent *event)
{
    if (enabled || event->isAutoRepeat())
        event->ignore();
    else
    {
        qDebug() << "Key released\n";
        int key = event->key();
        bool go = true;
        for (int i = 0; i < 2 && go; ++i)
            for (int j = 0; j < 6 && go; ++j)
                if (servo_control[i][j] == key)
                {
                    if (i)
                        controller.increase_move_factor(j);
                    else
                        controller.decrease_move_factor(j);
                    go = false;
                }
    }
}

void ArmControllerWindow::showEvent(QShowEvent *event)
{
    controller.synch();
    ui->entry_speed->setText(QString::number(default_speed));
    controller.set_update_frequency(default_speed);
    Q_UNUSED(event);
}

void ArmControllerWindow::on_btn_disconnect_clicked()
{
    coordinator->disconnect();
    close();
}

void ArmControllerWindow::on_btn_speed_clicked()
{
    int speed;
    if (ui->entry_speed->get_int(speed))
    {
        controller.set_update_frequency(speed);
        enabled = false;
        ui->entry_speed->setEnabled(enabled);
        ui->btn_speed->setEnabled(enabled);
    }
}

void ArmControllerWindow::on_btn_enable_clicked()
{
    enabled = !enabled;
    ui->entry_speed->setEnabled(enabled);
    ui->btn_speed->setEnabled(enabled);
}
