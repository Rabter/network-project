#include "intentry.h"

IntEntry::IntEntry(QWidget *parent): ValidEntry(parent)
{
    expression = "[+-]?(?:\\d+)";
}

bool IntEntry::get_int(int &num)
{
    if (validate())
    {
        make_correct();
        QString input = this->text();
        num = input.toInt();
        return true;
    }
    else
    {
        make_inorrect();
        return false;
    }
}
