#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "../coordinator/devicecoordinator.h"
#include "armcontrollerwindow.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btn_scan_clicked();
    void on_btn_connect_clicked();

private:
    Ui::MainWindow *ui;
    std::vector<Device> devices;
    DeviceCoordinator coordinator;
    ArmControllerWindow *controller;
};
#endif // MAINWINDOW_H
