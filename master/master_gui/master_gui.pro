QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ../coordinator/devicecoordinator.cpp \
    ../controller/armcontroller.cpp \
    armcontrollerwindow.cpp \
    intentry.cpp \
    main.cpp \
    mainwindow.cpp \
    ../../net/net.cpp \
    ../../net/nixnet.cpp \
    ../../socket/nixsocket.cpp \
    ../../socket/winsocket.cpp \
    ../../net/winnet.cpp \
    validentry.cpp

HEADERS += \
    ../coordinator/device.h \
    ../coordinator/devicecoordinator.h \
    ../controller/armcontroller.h \
    armcontrollerwindow.h \
    intentry.h \
    mainwindow.h \
    ../../net/net.h \
    ../../protocol.h \
    ../../socket/crossplatform_socket.h \
    ../../socket/nixsocket.h \
    ../../socket/socket.h \
    ../../socket/winsocket.h \
    validentry.h

FORMS += \
    armcontrollerwindow.ui \
    mainwindow.ui

win32: LIBS += -lws2_32
win32: LIBS += -liphlpapi
unix: LIBS += -lpthread

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
