#include <QString>
#include "mainwindow.h"
#include "ui_mainwindow.h"

#define TIME_TO_WAIT timeval{ 0, 750000 }

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btn_scan_clicked()
{
    ui->lw_devices->clear();
    ui->label_devices_status->setText("Scanning...");
    ui->label_devices_status->repaint();
    coordinator.scan_net(devices, TIME_TO_WAIT.tv_usec);
    for (const Device &dev: devices)
    {
        QString dev_string;
        if (dev.type == MECHANIZED_ARM)
            dev_string += "Mechanized arm: ";
        dev_string += QString(dev.name.c_str()) + " (" + QString(dev.ip.c_str()) + ')';
        ui->lw_devices->addItem(dev_string);
    }
    ui->label_devices_status->setText("Scanning completed");
}

void MainWindow::on_btn_connect_clicked()
{
    auto dev_list = ui->lw_devices->selectionModel()->selectedIndexes();
    if (dev_list.size() != 1)
    {
        ui->label_devices_status->setText("Select one device from the list");
        return;
    }
    if (coordinator.is_connected())
        coordinator.disconnect();
    if (coordinator.connect(devices[dev_list[0].data().value<int>()].ip))
        perror("Failed to connect.");
    controller = new ArmControllerWindow(this, &coordinator);
    controller->setModal(true);
    controller->show();
}
