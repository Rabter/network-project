#ifndef ARMCONTROLLERWINDOW_H
#define ARMCONTROLLERWINDOW_H

#include <QDialog>
#include "../coordinator/devicecoordinator.h"
#include "../controller/armcontroller.h"

namespace Ui {
class ControllerWindow;
}

class ArmControllerWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ArmControllerWindow(QWidget *parent, DeviceCoordinator *coordinator);
    ~ArmControllerWindow();

protected:
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;
    void showEvent(QShowEvent *event) override;

private slots:
    void on_btn_disconnect_clicked();

    void on_btn_speed_clicked();

    void on_btn_enable_clicked();

private:
    Ui::ControllerWindow *ui;
    DeviceCoordinator *coordinator;
    ArmController controller;
    static const int default_speed = 50;

    int servo_control[2][6] = {{ Qt::Key_Left, Qt::Key_Up, Qt::Key_Shift, Qt::Key_W, Qt::Key_A, Qt::Key_Q },
                               { Qt::Key_Right, Qt::Key_Down, Qt::Key_Control, Qt::Key_S, Qt::Key_D, Qt::Key_E }};
    bool enabled;
};

#endif // ARMCONTROLLERWINDOW_H
