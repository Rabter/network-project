#include "validentry.h"

ValidEntry::ValidEntry(QWidget *parent) : QLineEdit(parent)
{
    connect(this, SIGNAL(textChanged(QString)), this, SLOT(reset_validation()));
    valid = -1;
}

void ValidEntry::make_inorrect()
{
    this->setStyleSheet("background:#f88;");
}

void ValidEntry::make_correct()
{
    this->setStyleSheet("background:#fff;");
}

void ValidEntry::reset_validation()
{
    valid = -1;
}

void ValidEntry::set_reg_exp(QString exp)
{
    expression = exp;
}

int ValidEntry::validate()
{
    if (valid == -1)
    {
        QRegExp correct(expression);
        valid = correct.exactMatch(this->text());
    }
    return valid;
}
