#include <chrono>
#include <iostream>
#include "../../socket/crossplatform_socket.h"
#include "../../protocol.h"
#include "../../net/net.h"

#define TIME_TO_WAIT timeval{ 2, 0 }

int main(void)
{
//    Socket *sock = create_socket(AF_INET, SOCK_DGRAM);

//    int rc;
//    rc = sock->bind(MASTER_UDP_PORT);

//    int flag = 1;
//    rc = sock->setsockopt(SOL_SOCKET, SO_BROADCAST, &flag, sizeof(flag));

//    sock->sendto(broadcast_ip(local_ip(), subnet_mask()), DEVICE_UDP_PORT, MUSTER_MSG);

//    constexpr int len = sizeof(MUSTER_RESPONSE) + 1;
//    char buf[len];

//    fd_set readfds;

//    sock->FdZero(&readfds);
//    sock->FdSet(&readfds);

//    timeval lim = TIME_TO_WAIT;
//    long microseconds;

//    std::string dev_ip;
//    unsigned short dev_port;
//    std::chrono::time_point<std::chrono::system_clock> cur, begin = std::chrono::system_clock::now();
//    int responded;
//    do
//    {
//        if ((responded = sock->select(sock->fd() + 1, &readfds, NULL, NULL, &lim)))
//        {
//            sock->recvfrom(buf, sizeof(buf), 0, dev_ip, dev_port);
//            std::cout << "Recieved: " << buf << "\n";
//        }
//        cur = std::chrono::system_clock::now();
//        microseconds = std::chrono::duration_cast<std::chrono::microseconds>(cur - begin).count();
//    }while(microseconds < TIME_TO_WAIT.tv_sec * 1000000 + TIME_TO_WAIT.tv_usec || responded);

//    delete sock;

//    return 0;

    Socket *sock = create_socket(AF_INET, SOCK_STREAM);

    sock->connect("192.168.1.27", 31337);

    char msg[20];
    sock->send(GET " 0");
    sock->recv(msg, 20, 0);
    std::cout << msg;

    delete sock;
    return 0;
}
