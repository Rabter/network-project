TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    ../../net/net.cpp \
    ../../net/nixnet.cpp \
    ../../net/winnet.cpp \
    ../../socket/nixsocket.cpp \
    ../../socket/winsocket.cpp \
    ../controller/armcontroller.cpp \
    ../coordinator/devicecoordinator.cpp \
    main.cpp

HEADERS += \
    ../../net/net.h \
    ../../protocol.h \
    ../../socket/crossplatform_socket.h \
    ../../socket/nixsocket.h \
    ../../socket/socket.h \
    ../../socket/winsocket.h \
    ../controller/armcontroller.h \
    ../coordinator/device.h \
    ../coordinator/devicecoordinator.h

win32: LIBS += -lws2_32
win32: LIBS += -liphlpapi
unix: LIBS += -lpthread
