#ifndef ARMCONTROLLER_H
#define ARMCONTROLLER_H
#define SERVO_AMOUNT 6

#include <chrono>
#include <thread>
#include <mutex>
#include "../coordinator/devicecoordinator.h"

class ArmController
{
public:
    ArmController(DeviceCoordinator *coordinator, unsigned update_frequency = 1);

    void increase_move_factor(int servo_num);
    void decrease_move_factor(int servo_num);

    void synch();
    void set_update_frequency(unsigned frequeny);

    ~ArmController();

private:
    DeviceCoordinator *coordinator;
    int servo[SERVO_AMOUNT];
    int move[SERVO_AMOUNT];
    std::chrono::time_point<std::chrono::system_clock> update_at[SERVO_AMOUNT];

    std::thread *updater;
    std::mutex mtx_go, pauser;
    bool go;

    void update_servos();
    std::chrono::microseconds delay;
};

#endif // ARMCONTROLLER_H
