#include <thread>
#include "armcontroller.h"
#include "../../protocol.h"
#define SERVO_MIN 0
#define SERVO_MAX 180

ArmController::ArmController(DeviceCoordinator *coordinator, unsigned update_frequency): coordinator(coordinator)
{
    for (int i = 0; i < SERVO_AMOUNT; ++i)
    {
        servo[i] = 0;
        move[i] = 0;
        update_at[i] = std::chrono::system_clock::from_time_t(0);
    }
    go = true;
    delay = std::chrono::microseconds{ 1000000 / (update_frequency? update_frequency : 1)};
    updater = new std::thread(&ArmController::update_servos, this);
}

void ArmController::increase_move_factor(int servo_num)
{
    if (servo_num > -1 && servo_num < SERVO_AMOUNT)
        if (move[servo_num] < 1)
            ++move[servo_num];
}

void ArmController::decrease_move_factor(int servo_num)
{
    if (servo_num > -1 && servo_num < SERVO_AMOUNT)
        if (move[servo_num] > -1)
            --move[servo_num];
}

void ArmController::synch()
{
    pauser.lock(); // pause update_servos()
    for (int i = 0; i < SERVO_AMOUNT; ++i)
    {
        coordinator->send(GET " " + std::to_string(i));
        servo[i] = coordinator->get();
    }
    pauser.unlock(); // continue upate_servos()
}

void ArmController::set_update_frequency(unsigned frequeny)
{
    delay = std::chrono::microseconds{ 1000000 / (frequeny? frequeny : 1) };
}

ArmController::~ArmController()
{
    mtx_go.lock();
    go = false;
    mtx_go.unlock();
    if (updater->joinable())
        updater->join();
    delete updater;
}

void ArmController::update_servos()
{
    mtx_go.lock();
    while (go)
    {
        mtx_go.unlock();
        pauser.lock();
        for (int i = 0; i < SERVO_AMOUNT; ++i)
        {
            if (move[i] && std::chrono::system_clock::now() > update_at[i])
            {
                servo[i] += move[i];
                if (servo[i] > SERVO_MAX)
                    servo[i] = SERVO_MAX;
                else if (servo[i] < SERVO_MIN)
                    servo[i] = SERVO_MIN;
                else
                {
                    coordinator->send(SET " " + std::to_string(i) + ' ' + std::to_string(servo[i]));
                    update_at[i] = std::chrono::system_clock::now() + delay;
                }
            }
        }
        pauser.unlock();
        mtx_go.lock();
    }
    mtx_go.unlock();
}
